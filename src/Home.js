import BlogList from "./BlogList";
import useAxios from "./hooks/useAxios"

const Home = () => {
    const {data: blogs, isPending, error } = useAxios('https://jsonplaceholder.typicode.com/posts', 'get');
    return (
        <div className="home">
            {error && <div>{ error }</div>}
            { isPending && <div>Loading...</div> }
            {blogs && <BlogList blogs = {blogs} title = "All blogs!" />}
        </div>
    );
};
 
export default Home;
