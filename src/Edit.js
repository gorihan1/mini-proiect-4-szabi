import axios from "axios";
import { useState, useEffect} from "react";
import { useHistory, useParams } from "react-router-dom";
import useAxios from "./hooks/useAxios";



const Edit = () => {
    const { id } = useParams();
    const [title, setTitle] = useState('');
    const [body, setBody] = useState('');
    const [author, setAuthor] = useState('mario');
    const [isPending, setIsPending] = useState(false);
    const history = useHistory();
    const { data: blog } = useAxios('https://jsonplaceholder.typicode.com/posts/' + id, 'get');
    
    useEffect(() => {
        setTitle(blog && blog.title);
        setBody(blog && blog.body);
    }, [blog]);

    const handleEdit = (e) => {
        e.preventDefault();
        const blog = { title, body, author };
        setIsPending(true);

        axios.put('https://jsonplaceholder.typicode.com/posts/' + id, {
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(blog)
        }).then((response) => {
            console.log('edited the blog', response.data);
            setIsPending(false);
            history.go(-1);
        })
    }

    return (
        <div className="edit">
            <h2>Edit blog:</h2>
            <form onSubmit={handleEdit}>
                <label>Blog title: 
                    <input 
                    type="text"
                    required
                    value={title}
                    onChange= {(e) => setTitle(e.target.value)}
                    />
                </label>
                <label>Blog body: 
                    <textarea 
                        required
                        value={body}
                        onChange={(e) => setBody(e.target.value)}
                    ></textarea>
                </label>
                <label>Blog author: </label>
                <select
                    value={author}
                    onChange={(e) => setAuthor(e.target.value)}
                >
                    <option value="mario">mario</option>
                    <option value="yoshi">yoshi</option>
                </select>
                <button onClick={handleEdit}>Save</button> 
            </form>
        </div>
    );
}
 
export default Edit;