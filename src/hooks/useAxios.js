import axios from "axios";
import { useState, useEffect } from "react";

const useAxios = (url, method) => {
    const [data, setData] = useState(null);
    const [isPending, setIsPending] = useState(true);
    const [error, setError] = useState(null);

    useEffect( () => {

        axios[method](url)
        .then(response => {
            setData(response.data);
            setIsPending(false);
            setError(null);
        })
        .catch(err => {
            if (err.name === 'AbortError') {
                console.log('fetch aborted');
            }
            else{
                setError(err.message);
                setIsPending(false);
            }
        });

    }, [url, method]);

    return { data, isPending, error };
}

export default useAxios;