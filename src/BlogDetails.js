import axios from "axios";
import { Link, useHistory, useParams } from "react-router-dom";
import useAxios from "./hooks/useAxios";

const BlogDetails = () => {
    const { id } = useParams();
    const { data: blog, error, isPending } = useAxios('https://jsonplaceholder.typicode.com/posts/' + id, 'get');
    const history = useHistory();

    const handleClick= () => {
        axios.delete('https://jsonplaceholder.typicode.com/posts/' + blog.id )
        .then(() => {
            history.push('/');
        })
    }


    return (
        <div className="blog-details">
            { isPending && <div>Loading...</div> }
            { error && <div> { error }</div> }
            { blog && (
                <article>
                    <h2>{ blog.title }</h2>
                    <p>Written by { blog.author }</p>
                    <div>{ blog.body }</div>
                    <button onClick={handleClick}>Delete</button>
                    <Link to={`/edit/${blog.id}`}><button>Edit</button></Link>
                </article>
            )}
        </div>
    );
}
 
export default BlogDetails;